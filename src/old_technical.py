
# this are the remains of when I used the finta library, it turns out it is riddled with bugs and I'd rather not use it because future bias screwed me once already
from finta import TA

import luigi
import pandas as pd
import os
from model import DownloadPoloniex

import numpy as np
import pandas as pd
from sklearn import model_selection, linear_model, preprocessing, metrics


def truncate(df, key, bound):
    print('{} entries are more than {} in {}'.format((df[key] > bound).sum(), bound, key))
    df[key][df[key] > bound] = bound

    print('{} entries are less than {} in {}'.format((df[key] < -bound).sum(), -bound, key))
    df[key][df[key] < -bound] = -bound


def add_finta(df, mult=1):
    timeperiod = int(mult * 14)
    df['PERCENT_B'] = TA.PERCENT_B(df)
    
    df['VORTEX_VIm'] = TA.VORTEX(df).VIm
    df['VORTEX_VIp'] = TA.VORTEX(df).VIp
    truncate(df, 'VORTEX_VIm', 100)
    truncate(df, 'VORTEX_VIp', 100)
    
    df['VZO_{}'.format(mult)] = TA.VZO(df, timeperiod)
    df['EFI_{}'.format(mult)] = TA.EFI(df, timeperiod)
    truncate(df, 'EFI_{}'.format(mult), 25)
    
    df['EBBP_bull'] = TA.EBBP(df)['Bull.']
    truncate(df, 'EBBP_bull', 0.002)
    df['EBBP_bear'] = TA.EBBP(df)['Bear.']
    truncate(df, 'EBBP_bear', 0.002)
    
    df['EMV_{}'.format(mult)] = TA.EMV(df, timeperiod)
    truncate(df, 'EMV_{}'.format(mult), 5)

    df['COPP'] = TA.COPP(df)
    df['BASP_buy_{}'.format(timeperiod)] = TA.BASPN(df, int(30*mult))['Buy.']
    df['BASP_sell_{}'] = TA.BASPN(df, int(30*mult))['Sell.']
    
    df['WTO1_{}'.format(mult)] = TA.WTO(df, int(mult*10), int(mult*21))["WT1."]
    df['WTO2_{}'.format(mult)] = TA.WTO(df, int(mult*10), int(mult*21))["WT2."]

    df['FISH'] = TA.FISH(df)
    df['APZ_UPPER'] = TA.APZ(df)['UPPER'] - df.close
    df['APZ_LOWER'] = df.close - TA.APZ(df)['LOWER']
    df['SQZMI'] = TA.SQZMI(df)

    df['FVE_{}'.format(mult)] = TA.FVE(df, int(22*mult))

    df['VFI_{}'.format(mult)] = TA.VFI(df, int(130*mult))



# Don't know why, but they perform signficantly better :) :) :) :( I found out why. The finta library has future bias
class OldIndicators(luigi.Task):
    def requires(self):
        return DownloadPoloniex()

    def output(self):
        return luigi.LocalTarget('../data/features/technical_old.h5')

    def run(self):
        df = pd.read_json('../data/raw/BTC_ETH.json')

        df['target'] = df.close < df.close.shift(-1)

        # TODO: I just noticed that finta automatically names the series, so you can just aggregate with pd.concat([df,s], axis=1) and not give
        # a damn about explicitly naming the column
        # TODO: Add ALL the indicators
        # trend following
        df['TRIX'] = TA.TRIX(df)
        truncate(df, 'TRIX', 10000)
        df['ER'] = TA.ER(df)
        df['KAMA-'] = df.close - TA.KAMA(df)
        df['KAMA/'] = df.close / TA.KAMA(df)
        df['MACD_Signal'] = TA.VW_MACD(df, 12, 26).SIGNAL
        df['MACD_Signal_6'] = TA.VW_MACD(df, 6, 13).SIGNAL
        df['PPO_Signal'] = TA.PPO(df, 12, 26).SIGNAL
        df['PPO_Histo'] = TA.PPO(df, 12, 26).HISTO
        df['PPO_Signal_6'] = TA.PPO(df, 6, 13).SIGNAL
        df['PPO_Histo_6'] = TA.PPO(df, 6, 13).HISTO
        df['PPO_Signal_24'] = TA.PPO(df, 24, 52).SIGNAL
        df['PPO_Histo_24'] = TA.PPO(df, 24, 52).HISTO

        # momentum
        df['MOM_10'] = TA.MOM(df, 10)
        df['MOM_20'] = TA.MOM(df, 20)
        df['ROC'] = TA.ROC(df)
        df['ROC_6'] = TA.ROC(df, 6)
        df['ROC_3'] = TA.ROC(df, 3)
        df['RSI_14'] = TA.RSI(df)
        df['RSI_7'] = TA.RSI(df, 7)
        df['RSI_3'] = TA.RSI(df, 3)

        df['IFT_RSI'] = TA.IFT_RSI(df)
        truncate(df, 'IFT_RSI', 500)
        df['IFT_RSI_7'] = TA.IFT_RSI(df, 7)
        truncate(df, 'IFT_RSI_7', 500)

        df['TR'] = TA.TR(df)
        df['ATR'] = TA.ATR(df)
        df['ATR_7'] = TA.ATR(df, 7)
        df['ATR_4'] = TA.ATR(df, 4)

        df['SAR/'] = TA.SAR(df) / df.close
        df['SAR-'] = TA.SAR(df) - df.close
        df['BBWIDTH'] = TA.BBWIDTH(df)
        df['PERCENT_B'] = TA.PERCENT_B(df)

        df['DMI+'] = TA.DMI(df)['DI+']
        df['DMI-'] = TA.DMI(df)['DI-']
        df['DMI+_7'] = TA.DMI(df, 7)['DI+']
        df['DMI-_7'] = TA.DMI(df, 7)['DI-']
        df['DMI+_4'] = TA.DMI(df, 4)['DI+']
        df['DMI-_4'] = TA.DMI(df, 4)['DI-']

        df['STOCH_28'] = TA.STOCH(df, 28)
        df['STOCH'] = TA.STOCH(df)
        df['STOCH_7'] = TA.STOCH(df, 7)
        df['STOCH_3'] = TA.STOCH(df, 3)
        df['STOCHD'] = TA.STOCHD(df)
        df['STOCHRSI'] = TA.STOCHRSI(df)
        df['STOCHRSI_7'] = TA.STOCHRSI(df, 7, 7)
        df['STOCHRSI_4'] = TA.STOCHRSI(df, 4, 4)
        df['STOCHRSI_4_7'] = TA.STOCHRSI(df, 4, 7)
        df['STOCHRSI_7_4'] = TA.STOCHRSI(df, 7, 4)

        df['WILLIAMS_28'] = TA.WILLIAMS(df, 28)
        df['WILLIAMS'] = TA.WILLIAMS(df)
        df['WILLIAMS_7'] = TA.WILLIAMS(df, 7)
        df['WILLIAMS_4'] = TA.WILLIAMS(df, 4)

        df['UO'] = TA.UO(df)
        df['AO'] = TA.AO(df)
        df['AO_17'] = TA.AO(df, slow_period=17, fast_period=3)

        df['VORTEX_VIm'] = TA.VORTEX(df).VIm
        df['VORTEX_VIp'] = TA.VORTEX(df).VIp
        truncate(df, 'VORTEX_VIm', 100)
        truncate(df, 'VORTEX_VIp', 100)

        # moneyflow
        df['CHAIKIN'] = TA.CHAIKIN(df)
        truncate(df, 'CHAIKIN', 2500)
        df['MFI'] = TA.MFI(df) # better MACD
        df['MFI_7'] = TA.MFI(df,7)
        df['MFI_4'] = TA.MFI(df,4)
        df['WOBV_diff'] = TA.WOBV(df).diff()
        truncate(df, 'WOBV_diff', 2)
        df['WOBV_diff_1'] = df.WOBV_diff.shift(1)
        df['WOBV_diff_2'] = df.WOBV_diff.shift(2)
        df['WOBV_diff_3'] = df.WOBV_diff.shift(3)
        df['VZO_28'] = TA.VZO(df, 28)
        df['VZO'] = TA.VZO(df)
        df['VZO_7'] = TA.VZO(df, 7)
        df['VZO_4'] = TA.VZO(df, 4)
        df['EFI_28'] = TA.EFI(df, 28)
        truncate(df, 'EFI_28', 25)
        df['EFI'] = TA.EFI(df)
        truncate(df, 'EFI', 25)
        df['EFI_7'] = TA.EFI(df, 7)
        truncate(df, 'EFI_7', 25)
        df['EFI_4'] = TA.EFI(df, 4)
        truncate(df, 'EFI_4', 25)

        df['EBBP_bull'] = TA.EBBP(df)['Bull.']
        truncate(df, 'EBBP_bull', 0.002)
        df['EBBP_bear'] = TA.EBBP(df)['Bear.']
        truncate(df, 'EBBP_bear', 0.002)

        df['EMV'] = TA.EMV(df)
        truncate(df, 'EMV', 5)
        df['EMV_7'] = TA.EMV(df, 7)
        truncate(df, 'EMV_7', 5)
        df['EMV_4'] = TA.EMV(df, 4)
        truncate(df, 'EMV_4', 5)

        df['CCI'] = TA.CCI(df)
        truncate(df, 'CCI', 5)
        df['CCI_7'] = TA.CCI(df, 7)
        truncate(df, 'CCI_7', 5)
        df['CCI_4'] = TA.CCI(df, 4)
        truncate(df, 'CCI_4', 5)

        df['COPP'] = TA.COPP(df)
        df['BASP_buy'] = TA.BASPN(df)['Buy.']
        df['BASP_sell'] = TA.BASPN(df)['Sell.']
        df['BASP_buy_15'] = TA.BASPN(df, 15)['Buy.']
        df['BASP_sell_15'] = TA.BASPN(df, 15)['Sell.']

        df['CMO_18'] = TA.CMO(df, 18)
        df['CMO'] = TA.CMO(df)
        df['CMO_4'] = TA.CMO(df, 4)

        df['WTO'] = TA.WTO(df)["WT1."]
        df['WTO'] = TA.WTO(df)["WT2."]
        df['WTO_5'] = TA.WTO(df, 5, 11)["WT1."]
        df['WTO_5'] = TA.WTO(df, 5, 11)["WT2."]
        df['WTO_20'] = TA.WTO(df, 20, 32)["WT1."]
        df['WTO_20'] = TA.WTO(df, 20, 32)["WT2."]

        df['FISH'] = TA.FISH(df)
        df['APZ_UPPER'] = TA.APZ(df)['UPPER'] - df.close
        df['APZ_LOWER'] = df.close - TA.APZ(df)['LOWER']
        df['SQZMI'] = TA.SQZMI(df)

        df['FVE'] = TA.FVE(df)
        df['FVE_11'] = TA.FVE(df, 11)
        df['FVE_5'] = TA.FVE(df, 5)

        df['VFI'] = TA.VFI(df)
        df['VFI_75'] = TA.VFI(df, 75)
        df['VFI_35'] = TA.VFI(df, 35)

        df = df.drop(['volume', 'date', 'close', 'high', 'low', 'open', 'quoteVolume', 'weightedAverage'], axis=1)
        df = df.dropna()
        df = df[2000:]

        directory = os.path.dirname(self.output().path)
        if (not os.path.exists(directory)):
            os.mkdir(directory)
            
        df.to_hdf(self.output().path, key='BTC_ETH')
