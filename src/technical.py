import luigi
import pandas as pd
import os
from data import DownloadPoloniex
import talib

import numpy as np
import pandas as pd
from sklearn import model_selection, linear_model, preprocessing, metrics
from extramlutils import lightgbm_model


def add_patterns(df):
    df['CDL2CROWS'] = talib.CDL2CROWS(df.open, df.high, df.low, df.close)
    df['CDL3BLACKCROWS'] = talib.CDL3BLACKCROWS(df.open, df.high, df.low, df.close)
    df['CDL3INSIDE'] = talib.CDL3INSIDE(df.open, df.high, df.low, df.close)
    df['CDL3LINESTRIKE'] = talib.CDL3LINESTRIKE(df.open, df.high, df.low, df.close)
    df['CDL3OUTSIDE'] = talib.CDL3OUTSIDE(df.open, df.high, df.low, df.close)
    df['CDL3STARSINSOUTH'] = talib.CDL3STARSINSOUTH(df.open, df.high, df.low, df.close)
    df['CDL3WHITESOLDIERS'] = talib.CDL3WHITESOLDIERS(df.open, df.high, df.low, df.close)
    df['CDLABANDONEDBABY'] = talib.CDLABANDONEDBABY(df.open, df.high, df.low, df.close)
    df['CDLADVANCEBLOCK'] = talib.CDLADVANCEBLOCK(df.open, df.high, df.low, df.close)
    df['CDLBELTHOLD'] = talib.CDLBELTHOLD(df.open, df.high, df.low, df.close)
    df['CDLBREAKAWAY'] = talib.CDLBREAKAWAY(df.open, df.high, df.low, df.close)
    df['CDLCLOSINGMARUBOZU'] = talib.CDLCLOSINGMARUBOZU(df.open, df.high, df.low, df.close)
    df['CDLCONCEALBABYSWALL'] = talib.CDLCONCEALBABYSWALL(df.open, df.high, df.low, df.close)
    df['CDLCOUNTERATTACK'] = talib.CDLCOUNTERATTACK(df.open, df.high, df.low, df.close)
    df['CDLDARKCLOUDCOVER'] = talib.CDLDARKCLOUDCOVER(df.open, df.high, df.low, df.close)
    df['CDLDOJI'] = talib.CDLDOJI(df.open, df.high, df.low, df.close)
    df['CDLDOJISTAR'] = talib.CDLDOJISTAR(df.open, df.high, df.low, df.close)
    df['CDLDRAGONFLYDOJI'] = talib.CDLDRAGONFLYDOJI(df.open, df.high, df.low, df.close)
    df['CDLENGULFING'] = talib.CDLENGULFING(df.open, df.high, df.low, df.close)
    df['CDLEVENINGDOJISTAR'] = talib.CDLEVENINGDOJISTAR(df.open, df.high, df.low, df.close)
    df['CDLEVENINGSTAR'] = talib.CDLEVENINGSTAR(df.open, df.high, df.low, df.close)
    df['CDLGAPSIDESIDEWHITE'] = talib.CDLGAPSIDESIDEWHITE(df.open, df.high, df.low, df.close)
    df['CDLGRAVESTONEDOJI'] = talib.CDLGRAVESTONEDOJI(df.open, df.high, df.low, df.close)
    df['CDLHAMMER'] = talib.CDLHAMMER(df.open, df.high, df.low, df.close)
    df['CDLHANGINGMAN'] = talib.CDLHANGINGMAN(df.open, df.high, df.low, df.close)
    df['CDLHARAMI'] = talib.CDLHARAMI(df.open, df.high, df.low, df.close)
    df['CDLHARAMICROSS'] = talib.CDLHARAMICROSS(df.open, df.high, df.low, df.close)
    df['CDLHIGHWAVE'] = talib.CDLHIGHWAVE(df.open, df.high, df.low, df.close)
    df['CDLHIKKAKE'] = talib.CDLHIKKAKE(df.open, df.high, df.low, df.close)
    df['CDLHIKKAKEMOD'] = talib.CDLHIKKAKEMOD(df.open, df.high, df.low, df.close)
    df['CDLHOMINGPIGEON'] = talib.CDLHOMINGPIGEON(df.open, df.high, df.low, df.close)
    df['CDLIDENTICAL3CROWS'] = talib.CDLIDENTICAL3CROWS(df.open, df.high, df.low, df.close)
    df['CDLINNECK'] = talib.CDLINNECK(df.open, df.high, df.low, df.close)
    df['CDLINVERTEDHAMMER'] = talib.CDLINVERTEDHAMMER(df.open, df.high, df.low, df.close)
    df['CDLKICKING'] = talib.CDLKICKING(df.open, df.high, df.low, df.close)
    df['CDLKICKINGBYLENGTH'] = talib.CDLKICKINGBYLENGTH(df.open, df.high, df.low, df.close)
    df['CDLLADDERBOTTOM'] = talib.CDLLADDERBOTTOM(df.open, df.high, df.low, df.close)
    df['CDLLONGLEGGEDDOJI'] = talib.CDLLONGLEGGEDDOJI(df.open, df.high, df.low, df.close)
    df['CDLLONGLINE'] = talib.CDLLONGLINE(df.open, df.high, df.low, df.close)
    df['CDLMARUBOZU'] = talib.CDLMARUBOZU(df.open, df.high, df.low, df.close)
    df['CDLMATCHINGLOW'] = talib.CDLMATCHINGLOW(df.open, df.high, df.low, df.close)
    df['CDLMATHOLD'] = talib.CDLMATHOLD(df.open, df.high, df.low, df.close)
    df['CDLMORNINGDOJISTAR'] = talib.CDLMORNINGDOJISTAR(df.open, df.high, df.low, df.close)
    df['CDLMORNINGSTAR'] = talib.CDLMORNINGSTAR(df.open, df.high, df.low, df.close)
    df['CDLONNECK'] = talib.CDLONNECK(df.open, df.high, df.low, df.close)
    df['CDLPIERCING'] = talib.CDLPIERCING(df.open, df.high, df.low, df.close)
    df['CDLRICKSHAWMAN'] = talib.CDLRICKSHAWMAN(df.open, df.high, df.low, df.close)
    df['CDLRISEFALL3METHODS'] = talib.CDLRISEFALL3METHODS(df.open, df.high, df.low, df.close)
    df['CDLSEPARATINGLINES'] = talib.CDLSEPARATINGLINES(df.open, df.high, df.low, df.close)
    df['CDLSHOOTINGSTAR'] = talib.CDLSHOOTINGSTAR(df.open, df.high, df.low, df.close)
    df['CDLSHORTLINE'] = talib.CDLSHORTLINE(df.open, df.high, df.low, df.close)
    df['CDLSPINNINGTOP'] = talib.CDLSPINNINGTOP(df.open, df.high, df.low, df.close)
    df['CDLSTALLEDPATTERN'] = talib.CDLSTALLEDPATTERN(df.open, df.high, df.low, df.close)
    df['CDLSTICKSANDWICH'] = talib.CDLSTICKSANDWICH(df.open, df.high, df.low, df.close)
    df['CDLTAKURI'] = talib.CDLTAKURI(df.open, df.high, df.low, df.close)
    df['CDLTASUKIGAP'] = talib.CDLTASUKIGAP(df.open, df.high, df.low, df.close)
    df['CDLTHRUSTING'] = talib.CDLTHRUSTING(df.open, df.high, df.low, df.close)
    df['CDLTRISTAR'] = talib.CDLTRISTAR(df.open, df.high, df.low, df.close)
    df['CDLUNIQUE3RIVER'] = talib.CDLUNIQUE3RIVER(df.open, df.high, df.low, df.close)
    df['CDLUPSIDEGAP2CROWS'] = talib.CDLUPSIDEGAP2CROWS(df.open, df.high, df.low, df.close)
    df['CDLXSIDEGAP3METHODS'] = talib.CDLXSIDEGAP3METHODS(df.open, df.high, df.low, df.close)

    return df


# mult should be positive
def add_talib(df, mult=1.0):
    # timeperiod must be at least 2
    timeperiod = int(mult * 14) + 2

    upperband, middleband, lowerband = talib.BBANDS(df.close, nbdevup=int(mult * 2) + 0.5, nbdevdn=int(mult * 2) + 0.5)

    # these are the the overbought and oversold conditions cited on investopedia
    df['BBANDS_up_{}std'.format(mult)] = df.close > upperband
    df['BBANDS_down_{}std'.format(mult)] = df.close < lowerband
    df['BBWIDTH_{}std'.format(mult)] = upperband - lowerband

    df['HT_TREND_/'] = df.close / talib.HT_TRENDLINE(df.close)
    df['EMA/'] = df.close / talib.EMA(df.close)
    df['SAR_Signal'] = talib.SAR(df.high, df.low) > df.close

    # combine adx with DMI? like suggested in investopedia
    df['ADX_{}'.format(mult)] = talib.ADX(df.high, df.low, df.close, timeperiod=timeperiod)

    # Note that std varies, mean 0
    df['APO_{}'.format(mult)] = talib.APO(df.close, fastperiod=int(mult * 12) + 2, slowperiod=int(mult * 26) + 3)

    df['AROON_up'], df['AROON_down'] = talib.AROON(df.high, df.low)
    df['AROONOSC_{}'.format(mult)] = talib.AROONOSC(df.high, df.low, timeperiod)

    df['BOP'] = talib.BOP(df.close.shift(1), df.high, df.low, df.close)

    df['CCI_{}'.format(mult)] = talib.CCI(df.high, df.low, df.close, timeperiod=timeperiod)
    df['CMO_{}'.format(mult)] = talib.CMO(df.close, timeperiod=timeperiod)
    df['DX_{}'.format(mult)] = talib.DX(df.high, df.low, df.close, timeperiod=timeperiod)
    df['MFI_{}'.format(mult)] = talib.MFI(df.high, df.low, df.close, df.volume, timeperiod=timeperiod)

    df['MINUS_DI_{}'.format(mult)] = talib.MINUS_DI(df.high, df.low, df.close, timeperiod=timeperiod)
    # std varies, mean significantly nonzero
    df['MINUS_DM_{}'.format(mult)] = talib.MINUS_DM(df.high, df.low, timeperiod=timeperiod)
    # std varies, mean 0
    df['MOM_{}'.format(mult)] = talib.MOM(df.close, timeperiod=timeperiod)

    df['PLUS_DI_{}'.format(mult)] = talib.PLUS_DI(df.high, df.low, df.close, timeperiod=timeperiod)
    # std varies, mean significantly nonzero
    df['PLUS_DM_{}'.format(mult)] = talib.PLUS_DM(df.high, df.low, timeperiod=timeperiod)

    # std varies
    df['PPO_{}'.format(mult)] = talib.PPO(df.close, fastperiod=int(12 * mult) + 2, slowperiod=int(26 * mult) + 3)
    df['ROCR_{}'.format(mult)] = talib.ROCR(df.close, int(10 * mult) + 2)
    df['RSI_{}'.format(mult)] = talib.RSI(df.close, timeperiod=timeperiod)

    df['STOCH_k'], df['STOCH_d'] = talib.STOCH(df.high, df.low, df.close)
    df['STOCHRSI_k_{}'.format(mult)], df['STOCHRSI_d_{}'.format(mult)] = talib.STOCHRSI(df.close, timeperiod)

    df['ULTOSC_{}'.format(mult)] = talib.ULTOSC(df.high, df.low, df.close, timeperiod1=int(mult * 7) + 2,
                                                timeperiod2=timeperiod + 3,
                                                timeperiod3=int(mult * 28) + 4)
    df['WILLR_{}'.format(mult)] = talib.WILLR(df.high, df.low, df.close, timeperiod=timeperiod)
    # std varies, volume
    df['ADOSC_{}'.format(mult)] = talib.ADOSC(df.high, df.low, df.close, df.volume, slowperiod=int(mult * 10) + 2)

    # hilbert transform
    df['HT_DCPERIOD'] = talib.HT_DCPERIOD(df.close)
    df['HT_DCPHASE'] = talib.HT_DCPHASE(df.close)
    df['HT_PHASOR_inphase'], df['HT_PHASOR_quadrature'] = talib.HT_PHASOR(df.close)
    df['HT_SINE_sine'], df['HT_SINE_leadsine'] = talib.HT_SINE(df.close)
    df['HT_TRENDMODE'] = talib.HT_TRENDMODE(df.close)

    # Volatility
    df['NATR_{}'.format(mult)] = talib.NATR(df.high, df.low, df.close, timeperiod=timeperiod)


class Indicators(luigi.Task):
    def requires(self):
        return DownloadPoloniex()

    def output(self):
        return luigi.LocalTarget('../data/features/technical.h5')

    def run(self):
        directory = os.path.dirname(self.output().path)
        if not os.path.exists(directory):
            os.mkdir(directory)

        df = pd.read_json('../data/raw/BTC_ETH.json')

        df['target'] = df.close < df.close.shift(-1)

        add_patterns(df)

        df = df.drop(['volume', 'date', 'close', 'high', 'low', 'open', 'quoteVolume', 'weightedAverage'], axis=1)
        df = df.dropna()
        df = df[2000:]

        with self.output().temporary_path() as tmp_path:
            # the patterns featureset is basically useless, because it is so sparse
            df.to_hdf(tmp_path, key='patterns')

            df = pd.read_json('../data/raw/BTC_ETH.json')

            df['target'] = df.close - df.close.shift(-1)

            add_talib(df, 0.5)
            add_talib(df, 2)

            df = df.drop(['volume', 'date', 'close', 'high', 'low', 'open', 'quoteVolume', 'weightedAverage'], axis=1)
            df = df.dropna()
            df = df[2000:]

            df.to_hdf(tmp_path, key='indicators1')

            df = pd.read_json('../data/raw/BTC_ETH.json')

            df['target'] = np.log(df.close) - np.log(df.close.shift(-1))

            add_talib(df, 1)
            add_talib(df, 0.25)

            df = df.drop(['volume', 'date', 'close', 'high', 'low', 'open', 'quoteVolume', 'weightedAverage'], axis=1)
            df = df.dropna()
            df = df[2000:]

            df.to_hdf(tmp_path, key='indicators2')


class Logistic(luigi.Task):
    max_iter = luigi.IntParameter(default=30000)
    debug = luigi.BoolParameter(default=True)
    featureset = luigi.Parameter('indicators1')

    def requires(self):
        return Indicators()

    def output(self):
        return luigi.LocalTarget("../data/predictions/logistic_{}.h5".format(self.featureset))

    def run(self):
        directory = os.path.dirname(self.output().path)
        if (not os.path.exists(directory)):
            os.mkdir(directory)

        df = pd.read_hdf(Indicators().output().path, key=self.featureset, stop=(1000 if self.debug else None))
        df.target = df.target > 0
        x = np.array(df.drop('target', axis=1))

        for k in df:
            if df[k].dtype == 'float64':
                scaler = preprocessing.StandardScaler()
                df[k] = scaler.fit_transform(np.array(df[k]).reshape(-1, 1))

        model = linear_model.LogisticRegression(solver='saga', max_iter=self.max_iter)
        cv = model_selection.TimeSeriesSplit(n_splits=5)
        result = model_selection.cross_validate(model, x, np.array(df.target), cv=cv, return_estimator=True,
                                                scoring=['roc_auc', 'accuracy'])
        print(result)

        x = np.array(df.drop('target', axis=1))
        splits = cv.split(x)
        probs = np.zeros(len(df))
        probs[:] = np.nan
        for i, (l, test_idx) in enumerate(splits):
            probs[test_idx] = result['estimator'][i].predict_proba(x[test_idx])[:, 1]

        keynames = df.keys().drop(['target']).tolist()
        importances = np.mean(np.transpose([result['estimator'][i].coef_[0] for i in range(5)]), axis=1)
        importances = pd.Series(importances, index=keynames)

        with self.output().temporary_path() as tmp_path:
            s = pd.DataFrame({'series': pd.Series(probs),
                              'index': pd.Series(np.arange(df.index[0], df.index[-1] + 1, 1))})
            s.set_index('index').series.to_hdf(tmp_path, key='predictions')
            importances.to_hdf(tmp_path, key='importances')


class Regression(luigi.Task):
    max_iter = luigi.IntParameter(default=30000)
    debug = luigi.BoolParameter(default=True)
    featureset = luigi.Parameter('indicators1')
    rolling_std_timeframe = luigi.IntParameter(200)  # 25 gives best results
    model = luigi.Parameter(linear_model.HuberRegressor())
    scoring = ['neg_mean_squared_error', 'r2']
    steps_ahead = luigi.Parameter(1)

    def requires(self):
        return Indicators()

    def output(self):
        return luigi.LocalTarget("../data/predictions/regression_{}.h5".format(self.featureset))

    def run(self):
        directory = os.path.dirname(self.output().path)
        if not os.path.exists(directory):
            os.mkdir(directory)

        df = pd.read_hdf(Indicators().output().path, key=self.featureset, stop=(1000 if self.debug else None))

        keys = df.keys()[(df.dtypes == 'float64') & (df.keys() != 'target')]
        df[keys] = preprocessing.StandardScaler().fit_transform(df[keys])

        df.target = df.target.rolling(self.steps_ahead).sum().shift(1-self.steps_ahead)
        df.target = df.target / df.target.rolling(self.rolling_std_timeframe).std()
        df = df.dropna()
        x = np.array(df.drop('target', axis=1))

        cv = model_selection.TimeSeriesSplit(n_splits=5)
        models = lightgbm_model.cross_validate(self.model, x, df.target, cv=cv, scoring=self.scoring)

        preds = lightgbm_model.timeseries_oof(cv, models, df)

        with self.output().temporary_path() as tmp_path:
            preds.to_hdf(tmp_path, key='predictions')


