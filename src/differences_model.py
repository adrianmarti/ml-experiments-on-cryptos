
import pandas as pd
import numpy as np
from sklearn import model_selection
from sklearn import svm

import lightgbm as lgb
import luigi
import extramlutils
from data import DownloadPoloniex


param = {'n_estimators': 10000,
         'learning_rate': 0.005,
         'num_leaves': 27,
         'boosting': 'dart',
        #  'max_depth': 8,
         'early_stopping_round': 500}


def add_kth_log_diff(df, key, k):
    df['{}_log_diff_{}'.format(key, k)] = np.log(df[key])
    df['{}_log_diff_{}'.format(key, k)] = df['{}_log_diff_{}'.format(key, k)].diff(k)


class Diffed(luigi.Task):
    use_lgb = luigi.BoolParameter(default=True)
    epsilon = luigi.FloatParameter(default=0.0001)
    diffs = luigi.Parameter(default=[1,2,4,6,8,12,24,36,48])

    def requires(self):
        return DownloadPoloniex()
    
    def run(self):
        df = pd.read_json(self.input().path + "BTC_ETH.json")
        df['target'] = df.close < df.close.shift(-1)
        
        df['peak'] = df.high - (df.close + df.open) / 2
        df['bottom'] = (df.close + df.open) / 2 - df.low
        df['weekday'] = df.date.apply(lambda ts: ts.weekday())

        df.peak[df.peak < self.epsilon] = self.epsilon
        df.bottom[df.bottom < self.epsilon] = self.epsilon
        df.volume[df.volume < self.epsilon] = self.epsilon

        df = df[['target', 'close', 'volume']]

        # Da diffs
        for i in list(self.diffs):
            add_kth_log_diff(df, 'close', i)

        df = df.drop(['close', 'volume'], axis=1)

        # errors
        model = lgb.LGBMClassifier(niter=150)
        model.fit(df.drop('target', axis=1), df['target'])
        df['errors'] = (model.predict_proba(df.drop('target', axis=1))[:,0] - df['target']).shift(1)
        for i in range(5):
            df['errors_{}'.format(i)] = df.errors.shift(i+1)
        df = df.dropna()

        cv = model_selection.TimeSeriesSplit(n_splits=5)

        x = df.drop('target', axis=1)
        if self.use_lgb:
            model = lgb.LGBMClassifier(**param)
        else:
            model = svm.SVC()

        key_names = list(df.drop('target', axis=1).keys())
        models = extramlutils.lightgbm_model.cross_validate_lgb(model, np.array(x), np.array(df.target), cv=cv)
        print(extramlutils.get_best_iterations(models))
