
import json
import os
import urllib.request
import luigi


def download_pair(pair, path):
    request_string = "https://poloniex.com/public?command=returnChartData&currencyPair={}&start=1405699200&end=9999999999&period=1800".format(pair)
    contents = urllib.request.urlopen(request_string).read()
    with open(os.path.join(path, pair + '.json'), 'w') as f:
        f.write(contents.decode('utf-8'))


class DownloadPoloniex(luigi.Task):
    def output(self):
        return luigi.LocalTarget('../data/raw/')
    
    def run(self):
        with self.output().temporary_path() as temp_path:
            with open('../ressources/ticker.json') as f:
                pairs = json.load(f).keys()

            os.mkdir(temp_path)
            for pair in pairs:
                print("downloading {}...".format(pair))
                download_pair(pair, temp_path)
