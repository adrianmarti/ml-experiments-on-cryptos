
import luigi
import numpy as np
import pandas as pd
from sklearn import linear_model, preprocessing, model_selection, metrics
from hyperopt import hp, fmin, tpe
from math import log, pow
import technical


class Cryptoarchive(luigi.Task):
    time_interval = luigi.Parameter('1m')
    pair = luigi.Parameter('ETHBTC')

    def output(self):
        return luigi.LocalTarget('../data/cryptoarchive/{}_{}.csv'.format(self.pair, self.time_interval))
    
    def run(self):
        raise Exception('Download the data yourself from https://www.cryptoarchive.com.au/ and name it accordingly (in \
                         this case {}_{}.csv)'.format(self.pair, self.time_interval))


class ScaleX(luigi.Task):
    def output(self):
        return luigi.LocalTarget('../data/features/volatility_scaled.h5')
    
    def run(self):
        keys = ['timestamp','open','high','low','close','volume','TakerBuyQuoteAssetVolume',
                                   'TakerBuyBaseAssetVolume','QuoteAssetVolume','trades']
        df = pd.read_csv("../data/cryptoarchive/ETHBTC_1m.csv", sep='|', header=None, names=keys)
        df['target'] = pd.Series(np.log(df.close)).diff().shift(-1)
        df['std'] = np.square(df.target)
        df['std_cum'] = df['std'].cumsum()

        df['group'] = df['std_cum'] - df.std_cum % 2e-5
        df['minutes'] = 1
        
        def group_aggregation(group):
            return group.aggregate({'high': 'max', 'low': 'min', 'close': lambda x: x.iloc[-1], 'volume': 'sum', 'trades': 'sum', 'minutes': 'sum'})
        
        beautiful = df.groupby('group').apply(group_aggregation)
        beautiful.reset_index(inplace=True)
        with self.output().temporary_path() as tmp_path:
            beautiful.to_hdf(tmp_path, key='volatility')


def get_features(df, target_interval, feature_multipliers, rolling_std_window=None, enable_discount_factor=False):
    feature_scales = (feature_multipliers[0], feature_multipliers[0] * feature_multipliers[1])

    keys = df.keys().drop('next_return')
    if enable_discount_factor:
        # interpret target_interval as 50% decay interval
        alpha = pow(0.5, 1 / target_interval)
        ema = list(map(lambda n: (1 - alpha) * pow(alpha, n), range(target_interval)))
        log_diff = pd.Series(np.log(df.close)).diff()
        df['target'] = np.concatenate((np.convolve(ema, log_diff, mode='valid'), np.full(target_interval - 1, np.nan)))
    else:
        df['target'] = pd.Series(np.log(df.close)).diff(target_interval).shift(-target_interval) # let's be careful here... cause this can cause a lot of fun

    # list(map(lambda scale: technical.add_talib(df, scale), feature_scales))
    for i in (feature_scales[0], 2 * feature_scales[0], 5 * feature_scales[0],
              feature_scales[1], 2 * feature_scales[1], 5 * feature_scales[1]):
        i = int(i) + 1
        df['diff_{}'.format(i)] = pd.Series(np.log(df.close)).diff(i)

    df.drop(keys, axis=1, inplace=True)

    if rolling_std_window:
        df.target = df.target / df.target.rolling(rolling_std_window).std()

    df.dropna(inplace=True)

    keys = df.keys()[(df.dtypes == 'float64') & (df.keys() != 'target') & (df.keys() != 'next_return')]
    df[keys] = preprocessing.StandardScaler().fit_transform(df[keys]) 


yscaler_space = (
    hp.randint('target_interval', 96) + 2,
    (
        hp.lognormal('multiplier_0', log(1), 1),
        hp.lognormal('multiplier_1', log(1), 1)
    ),
    hp.randint('rolling_std_window_multiplier', 500) + 1
)

xscaler_space = (
    hp.randint('target_interval', 40) + 2,
    (
        hp.lognormal('multiplier_0', log(1), 1),
        hp.lognormal('multiplier_1', log(1), 1)
    )
)


def predict_oof(x, cv, estimators):
    splits = cv.split(x)
    preds = np.zeros(len(x))
    preds[:] = np.nan
    for i, (l, test_idx) in enumerate(splits):
        preds[test_idx] = estimators[i].predict(x[test_idx])
    return preds


def get_objective(df, enable_discount_factor):
    df['next_return'] = pd.Series(np.log(df.close)).diff().shift(-1)

    def objective(args):
        new_df = df.copy()
        print(args)
        if len(args[2]) == 2:
            epsilon, alpha, (target_interval, (m0,m1)) = args
            m_rolling_std = None
        else:
            epsilon, alpha, (target_interval, (m0,m1), m_rolling_std) = args
            m_rolling_std = m_rolling_std * target_interval
    
        alpha = max(1e-5, alpha) # otherwise sklearn can explode(i don't know why but it complains)

        get_features(new_df, target_interval, (min(m0 * target_interval, 100), min(m1, 5)),
                     m_rolling_std, enable_discount_factor=enable_discount_factor)
        #if len(args[2]) == 3:
        #new_df = new_df[::10]

        model = linear_model.HuberRegressor(epsilon=epsilon, alpha=alpha, fit_intercept=False)
        cv = model_selection.TimeSeriesSplit(n_splits=5)

        new_df['scaled_target'] = (np.array(new_df.target) - new_df.target.mean()) / new_df.target.std()
        x = np.array(new_df.drop(['target', 'scaled_target', 'next_return'], axis=1))
        result = model_selection.cross_validate(model, x, np.array(new_df.scaled_target), cv=cv, return_estimator=True)
        new_df['preds'] = predict_oof(x, cv, result["estimator"])

        buy_mask = new_df.preds * new_df.target.std() + new_df.target.mean() > 0
        sell_mask = new_df.preds * new_df.target.std() + new_df.target.mean() < 0
        log_returns = new_df.next_return[buy_mask].sum() - new_df.next_return[sell_mask].sum()
        new_df.dropna(inplace=True)
        print('r2 {}, mae {}, log ret {} \n'.format(metrics.r2_score(new_df.scaled_target, new_df.preds),
              metrics.mean_absolute_error(new_df.scaled_target, new_df.preds), log_returns))

        return -log_returns
    
    return objective


class Hyperopt(luigi.Task):
    scaler = luigi.Parameter('x')
    enable_discount_factor = luigi.Parameter(True)
    max_evals = luigi.IntParameter(200)

    def requires(self):
        return Cryptoarchive()
    
    def run(self):
        if self.scaler == 'x':
            print('reading xscaled data...')
            yield ScaleX()
            df = pd.read_hdf('../data/features/volatility_scaled.h5')
            sample_space = xscaler_space
        elif self.scaler == 'y':
            print('reading non-xscaled data...')
            keys = ['timestamp','open','high','low','close','volume','TakerBuyQuoteAssetVolume',
                                   'TakerBuyBaseAssetVolume','QuoteAssetVolume','trades']
            df = pd.read_csv("../data/cryptoarchive/ETHBTC_1m.csv", sep='|', header=None,
                             names=keys)
            sample_space = yscaler_space
        else:
            raise ValueError('scaler was neither x nor y')
        # we don't want no biased estimates, do we? So let's set aside a test set
        df = df.iloc[:int(len(df) * 0.8)]

        space = (1 + hp.lognormal('epsilon', log(0.35), 1), hp.lognormal('alpha', log(0.0001), 2), sample_space)

        best = fmin(get_objective(df, self.enable_discount_factor), space=space, algo=tpe.suggest, max_evals=self.max_evals)


class TimeRegressor(luigi.Task):
    def requires(self):
        return Cryptoarchive()
        
    def output(self):
        return luigi.LocalTarget('../data/predictions/timescaling.h5')
    
    def run(self):
        keys = ['timestamp','open','high','low','close','volume','TakerBuyQuoteAssetVolume',
                                   'TakerBuyBaseAssetVolume','QuoteAssetVolume','trades']
        df = pd.read_csv("../data/cryptoarchive/ETHBTC_1m.csv", sep='|', header=None,
                            names=keys)
        
        keys = df.keys()
        df['target'] = pd.Series(np.log(df.close)).diff(1).shift(-1) # let's be careful here... cause this can cause a lot of fun

        technical.add_talib(df, 0)
        df.drop(keys, axis=1, inplace=True)
        df.target = df.target / df.target.rolling(9).std()
        df.dropna(inplace=True)

        keys = df.keys()[(df.dtypes == 'float64') & (df.keys() != 'target')]
        df[keys] = preprocessing.StandardScaler().fit_transform(df[keys]) 
        # 98 hyperopt iterations until some exception ocurred(time_period was 0), rounded when it made sense, yielded all these hyperparameters
        # for some reason all three feature coefficients would be effectively 0, which sounds quite stupid
    
        df = df[::5]

        model = linear_model.HuberRegressor(epsilon=9.138, alpha=0.005)
        cv = model_selection.TimeSeriesSplit(n_splits=10)
        x = np.array(df.drop('target', axis=1))
        result = model_selection.cross_validate(model, x, np.array(df.target), cv=cv, scoring='r2', return_estimator=True)

        print('TRUE OUT OF SAMPLE IS', np.mean(result['test_score'][8:]))
        print('mean out of sample:', np.mean(result['test_score']))

        splits = cv.split(x)
        preds = np.zeros(len(df))
        preds[:] = np.nan
        for i, (l, test_idx) in enumerate(splits):
            preds[test_idx] = result['estimator'][i].predict(x[test_idx])

        keynames = df.keys().drop(['target']).tolist()
        importances = np.mean(np.transpose([result['estimator'][i].coef_ for i in range(5)]), axis=1)
        importances = pd.Series(importances, index=keynames)
        with self.output().temporary_path() as tmp_path:
            s = pd.DataFrame({'series': pd.Series(preds),
                        'index': pd.Series(np.arange(df.index[0],df.index[-1] + 1,1))})
            s.set_index('index').series.to_hdf(tmp_path, key='predictions')
            importances.to_hdf(tmp_path, key='importances')

