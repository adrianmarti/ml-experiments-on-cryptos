
# Installation:

1) Install the talib library; we will want to use the python-wrapper for it
2) Install hdf5
3) Run `pip install -r requirements.txt`

## In case your pc is weird like mine

For some reason, I had to clone tables(the thing that provides hdf5) and then build it with these commands in a fresh virtualenv:
```bash
pip install numpy cython
test/bin/python setup.py build_ext --inplace --hdf5=/usr/
test/bin/python setup.py install --hdf5=/usr/
```
These commands only worked for a fresh virtualenv for some reason(unless I somehow had the build_ext compilation step already executed in a different environment).


# Project structure

Models and data processing is located ins `src/`. Analysis of models and general visualization of data is in `notebooks/`. If you want to run jupyter, run it inside `notebooks/`
